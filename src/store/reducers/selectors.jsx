import { createSelector } from "reselect";
import { pokemonSelector as _pokemonSelector } from './pokemons/selector';

export const pokemonSelector = createSelector(pokemonSelector, _pokemonSelector)

