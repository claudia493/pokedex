import { createReducer } from '@reduxjs/toolkit';
import { guardarPokemon } from './actions';

const initialState = {
    pokemon: [],
};

const pokemonReducer = createReducer(initialState, (builder) => {
    builder
    .addCase(guardarPokemon, (state, { payload }) => ({
        ...state,
        pokemon: payload,
    })
)

})
export default pokemonReducer;