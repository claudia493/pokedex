import { createSelector } from 'reselect';

export const pokemonSelectors = createSelector((pokemonReducer) => pokemonReducer.pokemon, (pokemon) => pokemon)