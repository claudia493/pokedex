import { createAsyncThunk, createAction } from '@reduxjs/toolkit';
import { actionTypes } from '../types';
import axios from 'axios';
export const obtenerPokemones = createAction(actionTypes.OBTENER_POKEMONES);
export const guardarPokemon = createAction(actionTypes.GUARDAR_POKEMON);
export const obtenerPokemon = createAction(actionTypes.OBTENER_POKEMON);

export const pokemones = createAsyncThunk(
    actionTypes.OBTENER_POKEMONES,
    async (pokemon, { dispatch }) => {
        const results = new Promise ((resolve, rejected) => {
            (async () => {
                try {
                    const url = `https://pokeapi.co/api/v2/pokemon/${pokemon}`;
                    const res = await axios.get(url);
                    dispatch(guardarPokemon(res.data));
                    resolve('Exito')
                } catch (error) {
                    rejected({ msg: 'Algo salio mal' })
                }
            })()
        });

        return results;
    }
);