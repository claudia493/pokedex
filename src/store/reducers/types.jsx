const OBTENER_POKEMON = 'obtener/pokemon';
const OBTENER_POKEMONES = 'obtener/pokemones';
const GUARDAR_POKEMON = 'guardar/pokemon';

export const actionTypes = {
    OBTENER_POKEMONES,
    OBTENER_POKEMON,
    GUARDAR_POKEMON
}