import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';

import pokemonReducer from '../reducers/pokemons/index';

const appReducer = combineReducers({
    pokemon: pokemonReducer,
});

export default appReducer ;