import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Col, Image } from "react-bootstrap";
import classNames from "classnames";

import Logo from '../../assets/logo.png';
import Avatar from '../../assets/avatar.png';
import Logout from '../../assets/Icons/Logout.svg'
class SideBar extends React.Component {
  render() {
    return (
      <div className={classNames("sidebar", { "is-open": this.props.isOpen })}>
        <div className="sidebar-header">
          <Button
            variant="link"
            onClick={this.props.toggle}
            style={{ color: "#fff" }}
            className="mt-4"
          >
            <FontAwesomeIcon icon={faTimes} pull="right" size="xs" />
          </Button>
            <Row className="rowLogo">
                <Col xs={12} md={12}>
                    <Image className="logo" src={Logo}/>
                </Col>
            </Row>
            <Row className="rowAvatar">
                <Col xs={12} md={12}>
                    <Image className="avatar" src={Avatar} roundedCircle />
                </Col>
            </Row>
            <Row>
                <Col className="text-center text-white mt-4" xs={12} md={12}>
                    <h4>ASHK123</h4>
                    <h6>Level 1</h6>
                </Col>
            </Row>
            <Row>
                <Col className="text-center text-white mt-4" xs={12} md={12}>
                    <p className="text">"Work hard in your test"</p>
                </Col>
            </Row>
            <Row className="rowLogout">
                <Col className="text-center text-white mt-4" xs={12} md={12}>
                <Image className="logoutImage" src={Logout}/>
                  <Button className="logout" size="lg" active>
                    LOG OUT
                  </Button>
                </Col>
            </Row>
        </div>
      </div>
    );
  }
}

export default SideBar;