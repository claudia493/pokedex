import React, { useState, useEffect } from "react";
import { Container, Card, CardGroup } from "react-bootstrap";
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { Navbar, Button, Form, Col, Row, Image} from "react-bootstrap";
import Search from '../../assets/Icons/Search.svg';
import pokemones from '../../store/reducers/pokemons/actions';

const Content = (props) => {
  const [pokemon, setPokemon] = useState("");
  const [pokemonData, setPokemonData] = useState([]);
  const [pokemons, setPokemons] = useState([]);
  const [hidden, setHidden] = useState(false)

  const handleChange = (e) => {
    setPokemon(e.target.value.toLowerCase());
  };
  
  const handleSubmit = (e) => {
    e.preventDefault();
    getPokemon();
    setHidden(true)
  };

  const getPokemon = async () => {
    const toArray = [];
    try {
      const url = `https://pokeapi.co/api/v2/pokemon/${pokemon}`;
      const res = await axios.get(url);
      toArray.push(res.data);
      setPokemonData(toArray);
      
    } catch (e) {
      console.log(e);
    }
  };


    return (
      <Container
        fluid
      >
        <Navbar
        className="navbar p-3 mb-5"
        expand
        toggle={props.toggle}
      >
        <Button className="arrowLeft"  onClick={props.toggle}>
          <FontAwesomeIcon icon={faArrowLeft} />
        </Button>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Form className="formSearch" onSubmit={handleSubmit}>
            <Row>
              <Col sm={8} className="my-1">
                <Form.Control className="inputSearch" placeholder="Search" onChange={handleChange}/>{' '}
              </Col>
              <Col sm={4} className="my-1">
              <Button className="btnSearch" type="submit">
                  <Image className="searchImage" src={Search}/>
                </Button>
              </Col>
            </Row>
          </Form>
        </Navbar.Collapse>
      </Navbar>
      {props.setPokemonData.map((data) => {
        return(
          <div>
            <CardGroup className="cardGroup cardGroupDefault" hidden={hidden}>
              <Card>
                <Card.Body className="text-start">
                  <Card.Title className="p-4">{data.name}</Card.Title>
                    <Card.Text>
                        {data.url}
                    </Card.Text>
                </Card.Body>
              </Card>
            </CardGroup>
          </div>
        )
      })}
      {pokemonData.map((data) => {
        return(
          <div>
          <CardGroup className="cardGroup">
              <Card>
                <div>
                  <Card.Img variant="top" src={data.sprites.front_default} />
                  <div className="divImages">
                    <Card.Img className="front" variant="top" src={data.sprites.front_default} />{' '}
                    <Card.Img className="back" variant="top" src={data.sprites.back_default} />
                  </div>
                </div>
              </Card>
            </CardGroup>
            <CardGroup className="cardGroup content">
              <Card>
                <Card.Body className="text-start">
                  <Card.Title className="pokemonName p-4">{data.name}</Card.Title>
                    <div className="divTypes">
                    <Card.Text className={data.types[0].type.name}>
                        {data.types[0].type.name}
                    </Card.Text>
                    { data.types[1]?.type.name ? 
                    <Card.Text className={data.types[1].type.name}>
                        {data.types[1].type.name}
                    </Card.Text> :
                    null
                    }
                  </div> 
                  <div className="cardDescription">
                    <Card.Title className="pokemonNumber">Pokedex number</Card.Title>
                    <Card.Text className="pNumber p-0">
                        {data.id}
                    </Card.Text>
                    <hr />
                    <Card.Title className="pokemonNumber">Height</Card.Title>
                    <Card.Text className="pNumber p-0">
                        {data.height}
                    </Card.Text>
                    <hr />
                    <Card.Title className="pokemonNumber">Weight</Card.Title>
                    <Card.Text className="pNumber p-0">
                        {data.weight}
                    </Card.Text>
                    <hr />
                    <div className="divImages">
                      <Card.Img className="front" variant="top" src={data.sprites.front_shiny} />{' '}
                      <Card.Img className="back" variant="top" src={data.sprites.back_shiny} />
                    </div>
                  </div>
                </Card.Body>
              </Card>
            </CardGroup>
          </div>
          )
        })}
      </Container>
    );
  }

export default Content;